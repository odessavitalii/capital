$(window).on('load', function() {
	setTimeout(() => {
		$('body').removeClass('is-preload');
	}, 1000);
});

$('document').ready(function() {
	init();
});

$(window).resize(function () {
	init();
});

function init() {

	const $window = $(window);
	const $body = $('body');
	const $header = $('.header');
	const $secMain = $('.sec-main');
	const $animElems  =$('[data-aos]');

	/*START AOS JS*/
	if ($animElems.length) {
		setTimeout(() => {
			AOS.init({
				easing: 'ease-out-back',
				duration: 1000,
				once: true
			});
		}, 1000);
	}
	/*END AOS JS*/

	$(window).scroll(function (e) {

		if ($(document).scrollTop() > 10) {
			$header.addClass('sticky')
		} else {
			$header.removeClass('sticky')
		}

	});
}
